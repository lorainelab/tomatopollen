#!/usr/bin/env python

"""
Replace subject title with AGI code to enable easier matching between tomato
and Arabidopsis gene ids.
Replace query id with Sol genomics id only, removing extra text.

Run like this:

gunzip -c blastp_tair10.tsv.gz | tidyBlastpResults.py  > blastp_tair10_tidy.tsv.gz
"""

import fileinput,sys,os,re

agi_reg=re.compile(r'AT[1-5CM]G\d{5}\.\d+$')
sol_reg=re.compile(r'Solyc\d\dg\d{6}\.\d.\d$')

def main():
    for line in fileinput.input():
        toks=line.rstrip().split('\t')
        query_id=toks[1].split()[0]
        if not sol_reg.search(query_id): # should never happen
            if query_id=='q.id': # except on first line
                sys.stdout.write(line)
                continue
            else:
                raise ValueError("Unrecognized query id: %s on %s"%(query_id,line))
        else:
            toks[1]=query_id
        subject_title=toks[-1]
        agi=subject_title.split()[0]
        if not agi=='NA':
            if not agi_reg.search(agi):  # should never happen
                raise ValueError("Unrecognized subject id: %s on %s"%(agi,line))
        toks[-1]=agi
        newline='\t'.join(toks)+'\n'
        sys.stdout.write(newline)

if __name__ == '__main__':
    main()



