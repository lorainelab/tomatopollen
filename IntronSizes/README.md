# Check your intron sizes before you align your RNA-Seq data

Many spliced alignment tools limit their alignment search space based on a maximum intron size setting.

For most such tools, the default setting for maximum intron size is very large.

To set this size, you should examine the intron size distribution for your reference genome and pick something reasonable. 

The code in this directory shows how we did this for a tomato reference genome. 