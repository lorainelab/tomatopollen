makeClusterPlot=function(hc,lab=hc$labels,
                         lab.col=rep(1,length(hc$labels)),
                         hang=0.1,axes=F, ...) {
  y = rep(hc$height,2)
  x = as.numeric(hc$merge)
  y = y[which(x<0)]
  x = x[which(x<0)]; x=abs(x)
  y = y[order(x)]; x = x[order(x)]
  plot(hc,labels=FALSE,hang=hang,axes=axes, ...)
  text(x=x, y=y[hc$order]-(max(hc$height)*hang),
       labels=lab[hc$order], col=lab.col[hc$order],
       srt=90, adj=c(1,0.5),xpd=NA, ... )
}

# source ProjectVariables.R before using this
# depends on colors being already defined
# v - vector of sample labels
getColors=function(v){
  cn=grep('C',v)
  tr=grep('T',v)
  toReturn=character(length(v))
  toReturn[cn]=cn.color
  toReturn[tr]=tr.color
  return(toReturn)
} 